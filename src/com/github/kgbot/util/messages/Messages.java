package com.github.kgbot.util.messages;

import org.pircbotx.Channel;

import com.github.kgbot.listeners.commands.core.BotUser;
import com.github.kgbot.main.KGBot;
import com.github.kgbot.util.enums.ColorFormat;


public class Messages
{
    public static String convertColors(ColorFormat format, String message)
    {
        //uncomment to enable colors
        /*message = format + message;
        message = message.replace("&n", Colors.NORMAL + format);
        message = message.replace("&b", Colors.BOLD);
        message = message.replace("&u", Colors.UNDERLINE);
        message = message.replace("&r", Colors.REVERSE);
        message = message.replace("&16", Colors.LIGHT_GRAY);
        message = message.replace("&15", Colors.DARK_GRAY);
        message = message.replace("&14", Colors.MAGENTA);
        message = message.replace("&13", Colors.BLUE);
        message = message.replace("&12", Colors.CYAN);
        message = message.replace("&11", Colors.TEAL);
        message = message.replace("&10", Colors.GREEN);
        message = message.replace("&9", Colors.YELLOW);
        message = message.replace("&8", Colors.OLIVE);
        message = message.replace("&7", Colors.PURPLE);
        message = message.replace("&6", Colors.BROWN);
        message = message.replace("&5", Colors.RED);
        message = message.replace("&4", Colors.DARK_GREEN);
        message = message.replace("&3", Colors.DARK_BLUE);
        message = message.replace("&2", Colors.BLACK);
        message = message.replace("&1", Colors.WHITE);
        */
        
        message = message.replace("&n", "");
        message = message.replace("&b", "");
        message = message.replace("&u", "");
        message = message.replace("&r", "");
        message = message.replace("&16", "");
        message = message.replace("&15", "");
        message = message.replace("&14", "");
        message = message.replace("&13", "");
        message = message.replace("&12", "");
        message = message.replace("&11", "");
        message = message.replace("&10", "");
        message = message.replace("&9", "");
        message = message.replace("&8", "");
        message = message.replace("&7", "");
        message = message.replace("&6", "");
        message = message.replace("&5", "");
        message = message.replace("&4", "");
        message = message.replace("&3", "");
        message = message.replace("&2", "");
        message = message.replace("&1", "");
        
        return message;
    }

    public static void respond(Channel channel, ColorFormat format, BotUser user, String message)
    {
        if (channel != null)
            channel.send().message(convertColors(format, user.getNick() + ": " + message));
        else if (user.getIrcUser() != null) user.getIrcUser().send().message(convertColors(format, user.getNick() + ": " + message));
    }

    public static void respondRaw(Channel channel, ColorFormat format, BotUser user, String message)
    {
        if (channel != null)
            channel.send().message(user.getNick() + ": " + message);
        else if (user.getIrcUser() != null)  user.getIrcUser().send().message(user.getNick() + ": " + message);
    }

    public static void sendMessage(Channel[] channels, ColorFormat format, String message)
    {
        for (Channel channel : channels)
            sendMessage(channel, format, message);
    }
    
    public static void sendMessage(Channel channel, ColorFormat format, String message)
    {
        channel.send().message(convertColors(format, message));
    }

    public static void sendNotice(ColorFormat format, BotUser user, String message)
    {
        if (user.getIrcUser() != null)
            user.getIrcUser().send().notice(convertColors(format, message));
        else KGBot.getMainChannel().send().message(convertColors(format, message));
    }
}
