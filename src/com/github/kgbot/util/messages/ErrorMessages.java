package com.github.kgbot.util.messages;

import org.pircbotx.Channel;

import com.github.kgbot.listeners.commands.core.BotUser;
import com.github.kgbot.util.enums.ColorFormat;

public class ErrorMessages
{
    public static void alreadyActivated(Channel channel, BotUser user)
    {
        Messages.respond(channel, ColorFormat.ERROR, user, "I'm already activated, retard!");
    }

    public static void alreadyDeactivated(Channel channel, BotUser user)
    {
        Messages.respond(channel, ColorFormat.ERROR, user, "I'm already deactivated, retard!");
    }

    public static void invalidNumber(Channel channel, BotUser user)
    {
        Messages.respond(channel, ColorFormat.ERROR, user, "One or more of those numbers is invalid, retard!");
    }

    public static void invalidOperation(Channel channel, BotUser user)
    {
        Messages.respond(channel, ColorFormat.ERROR, user, "That operation is invalid, retard!");
    }

    public static void invalidQuoteUsage(Channel channel, BotUser user)
    {
        Messages.respond(channel, ColorFormat.ERROR, user, "Invalid usage. Check a list of commands with &b!quotes help");
    }

    public static void minOverMax(Channel channel, BotUser user)
    {
        Messages.respond(channel, ColorFormat.ERROR, user, "The specified minimum is over the specified maximum!");
    }

    public static void notCommand(Channel channel, BotUser user, String command)
    {
        Messages.respond(channel, ColorFormat.ERROR, user, "Command &b'" + command + "'&n not found. Type &b!help&n for more info.");
    }

    public static void notEnoughQuotes(Channel channel, BotUser user)
    {
        Messages.respond(channel, ColorFormat.ERROR, user, "There are not that many quotes! Check the quotes list with &b!quotes");
    }

    public static void notFoundPlayer(Channel channel, BotUser user, String player)
    {
        Messages.respond(channel, ColorFormat.ERROR, user, "Couldn't find the player &b'" + player + "'");
    }

    public static void notHighEnoughAccessLevel(Channel channel, BotUser user)
    {
        Messages.respond(channel, ColorFormat.ERROR, user, "You do not have the required access level to perform that command.");
    }

    public static void notLink(Channel channel, BotUser user, String link)
    {
        Messages.respond(channel, ColorFormat.ERROR, user, "&b'" + link + "' &nis not a valid link, retard! Get a list of links with &b!links");
    }

    public static void quoteAlreadyExists(Channel channel, BotUser user)
    {
        Messages.respond(channel, ColorFormat.ERROR, user, "That quote already exists, retard!");
    }

    public static void quoteDoesntExist(Channel channel, BotUser user)
    {
        Messages.respond(channel, ColorFormat.ERROR, user, "That quote doesn't exist, retard!");
    }

    public static void invalidUrl(Channel channel, BotUser user, String url)
    {
        Messages.respond(channel, ColorFormat.ERROR, user, "Invalid URL: " + url);
    }

    public static void notEnoughMoney(Channel channel, BotUser user)
    {
        Messages.respond(channel, ColorFormat.ERROR, user, "You don't have enough money to do that!");
    }
    
    public static void invalidUser(Channel channel, BotUser user, String other)
    {
        Messages.respond(channel, ColorFormat.ERROR, user, other + " doesn't exist in my database!");

    }

    public static void notChallengedToFlip(Channel channel, BotUser user)
    {
        Messages.respond(channel, ColorFormat.ERROR, user, "Nobody challenged you to a coin flip, retard!");
    }

    public static void invalidPoll(Channel channel, BotUser user)
    {
        Messages.respond(channel, ColorFormat.ERROR, user, "There is no poll with that ID!");
    }

    public static void pollAlreadyStarted(Channel channel, BotUser user)
    {
        Messages.respond(channel, ColorFormat.ERROR, user, "That poll is already running!");
    }

    public static void invalidPollChoice(Channel channel, BotUser user)
    {
        Messages.respond(channel, ColorFormat.ERROR, user, "That is an invalid choice!");
    }

    public static void notEnoughMoneyOther(Channel channel, BotUser user, String other)
    {
        Messages.respond(channel, ColorFormat.ERROR, user, other + " doesn't have enough money to do that!");
    }

    public static void invalidItemId(Channel channel, BotUser user)
    {
        Messages.respond(channel, ColorFormat.ERROR, user, "That's an invalid item ID!");
    }

    public static void insultAlreadyExists(Channel channel, BotUser user)
    {
        Messages.respond(channel, ColorFormat.ERROR, user, "That insult already exists, retard!");
    }

    public static void insultDoesntExist(Channel channel, BotUser user)
    {
        Messages.respond(channel, ColorFormat.ERROR, user, "That insult doesn't exist, retard!");
    }

    public static void notEnoughInsults(Channel channel, BotUser user)
    {
        Messages.respond(channel, ColorFormat.ERROR, user, "There are not that many insults!");
    }

    public static void invalidInsultUsage(Channel channel, BotUser user)
    {
        Messages.respond(channel, ColorFormat.ERROR, user, "Invalid usage. Check a list of commands with &b!insults help");
    }
}
