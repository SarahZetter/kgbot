package com.github.kgbot.util;

import java.util.Date;
import java.util.Random;

public class NumberMethods
{
    private static Random random = new Random(new Date().getTime());
    
    public static Random getRandom()
    {
        return random;
    }

    public static boolean isDouble(String string)
    {
        try
        {
            Double.valueOf(string);
            return true;
        } catch (Exception e)
        {
            return false;
        }
    }

    public static boolean isInteger(String string)
    {
        try
        {
            Integer.valueOf(string);
            return true;
        } catch (Exception e)
        {
            return false;
        }
    }
    
    public static String secondsToHHMMSS(int input)
    {

        int hours = input / 3600,
        remainder = input % 3600,
        minutes = remainder / 60,
        seconds = remainder % 60;
    
        if (input == 0) return "0";
        return 
        (hours > 0 ? hours + " hour" + (hours > 1 ? "s " : " ") : "") + 
        (minutes > 0 ? minutes + " minute" + (minutes > 1 ? "s " : " ") : "") + 
        (seconds > 0 ? seconds + " second" + (seconds > 1 ? "s" : "") : "");
    }

    public static int negativeRandom(int low, int high)
    {

        if (low == high) return low;

        int tmp = low;
        low = high;
        high = tmp;

        int diff = high - low;

        int i = random.nextInt(diff + 1);

        return i + low;
    }
    
    public static int roll(int min, int max)
    {
        boolean negativeInvolved = min < 0 || max < 0;

        if (!negativeInvolved && min > max || negativeInvolved && min < max)
        {
            return -1;
        }
        
        if (negativeInvolved) return negativeRandom(min, max);
        else return (min + (int) (Math.random() * ((max - min) + 1)));
    }
    
    //Arguments need to be supplied like this: distanceBetweenPoints(new Integer[] {x, y, z}, new Integer[] {x, y, z})
    public static double distanceBetweenPoints(Double[] A, Double[] B)
    {
        //Distance Between Points [A(x1, y1, z1), B(x2, y2, z2)] = √[(x2 - x1)2 + (y2 - y1)2 + (z2 - z1)2]
        return Math.sqrt(Math.pow(B[0] - A[0], 2) + Math.pow(B[1] - A[1], 2) + Math.pow(B[2] - A[2], 2));
    }
    
    public static double distanceBetweenPoints(String[] A, String[] B)
    {
        return distanceBetweenPoints(new Double[] {
        Double.valueOf(A[0]),
        Double.valueOf(A[1]),
        Double.valueOf(A[2])
        }, new Double[] {
        Double.valueOf(B[0]),
        Double.valueOf(B[1]),
        Double.valueOf(B[2])
        });
    }
    
    public static String coordsToString(Double[] coords)
    {
        double x = coords[0];
        double y = coords[1];
        double z = coords[2];
        return "(" + (int) x + "," + (int) y + "," + (int) z + ")";
    }
    
    public static String coordsToString(String[] coords)
    {
        return coordsToString(new Double[] {
        Double.valueOf(coords[0]),
        Double.valueOf(coords[1]),
        Double.valueOf(coords[2])
        });
    }
}
