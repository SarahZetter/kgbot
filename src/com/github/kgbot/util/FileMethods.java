package com.github.kgbot.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class FileMethods
{
    public static ArrayList<String> readFileFromURL(String url)
    {
        try {
            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
    
            ArrayList<String> contents = new ArrayList<>();
            String line = "";
            
            while ((line = in.readLine()) != null)
                contents.add(line);
            
            in.close(); 
            
            return contents;
        } catch (Exception e)
        {
            e.printStackTrace();
        }
        
        return null;
    }
    
    public static String listToString(ArrayList<String> list)
    {
        String returnValue = "";
       
        for (String line : list)   
        {
            returnValue += line + "\n";
        }
        
        return returnValue;
    }
}
