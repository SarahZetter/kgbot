package com.github.kgbot.util.enums;

public enum Link
{
    NORBO11_STREAM("stream", "Norbo11's stream", "http://www.twitch.tv/norbo123"), 
    
    REPO("repo", "KGBot github repository", "https://github.com/Norbo11/KGBot"),

    PIRCBOTX_HOME("pircbotx", "PircBotX project page", "http://code.google.com/p/pircbotx/"), 
    PIRCBOTX_JAVADOC("javadoc", "PircBotX JavaDoc", "http://site.pircbotx.googlecode.com/hg-history/1.7/apidocs/index.html"),

    BUXVILLE_HOME("buxville", "Buxville home page", "http://www.buxville.net/xf/"),
    BUXVILLE_FORUM("forum", "Buxville forum", "http://www.buxville.net/xf/forum/"), 
    BUXVILLE_WIKI("wiki", "Buxville wiki", "http://www.buxville.net/xf/wiki/index/"), 
    BUXVILLE_LOTMANAGER("lotmanager", "Buxville lotmanager", "http://www.buxville.net/xf/nodes/buxlotmanager.php"),

    QUOTES("quotes", "Random quotes for KGB", "http://37.128.190.229/quotes.php"),
    ANTHEM("anthem", "GANGNAM STYLE", "http://www.youtube.com/watch?v=9bZkp7q19f0"),
    WEBSITE("web", "Our Website", "http://www.buxboss.net"),
    KGBWEB("kgbweb", "KGBWeb", "http://kgbweb.buxboss.net"),
    PASTIE("pastie", "Our Pastebin", "http://pastebin.com/u/TrueForceOfBux");
    
    private Link(String shortName, String name, String url)
    {
        this.shortName = shortName;
        this.name = name;
        this.url = url;
    }

    private String shortName;

    private String name;

    private String url;

    public static Link getByShortName(String shortName)
    {
        for (Link link : values())
        {
            if (link.shortName.equalsIgnoreCase(shortName)) return link;
        }
        return null;
    }

    public String getName()
    {
        return name;
    }

    public String getShortName()
    {
        return shortName;
    }

    public String getUrl()
    {
        return url;
    }

    @Override
    public String toString()
    {
        return "&4(" + shortName + ") &14" + name + ": &b" + url;
    }
}
