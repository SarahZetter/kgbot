package com.github.kgbot.util.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Database
{
    private static final String DATABASE_HOST = "37.77.82.170";
    private static final String DATABASE_USER = "kgbot";
    private static final String DATABASE_PASS = "kgbot123";
    private static final String DATABASE_NAME = "kgbot";
    public static final String TABLE_QUOTES = "quotes";
    public static final String TABLE_USERS = "users";
    public static final String TABLE_INSULTS = "insults";
    public static Connection database;
    
    
    static {
        try
        {
            Class.forName("com.mysql.jdbc.Driver");
            database = DriverManager.getConnection("jdbc:mysql://" + DATABASE_HOST + "/" + DATABASE_NAME, DATABASE_USER, DATABASE_PASS);
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public static void execute(String query)
    {
        try
        {
            database.prepareStatement(query).execute();
        } catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    //Remember to disconnect after use
    public static ResultSet executeQuery(String query)
    {
        try {
            return database.prepareStatement(query).executeQuery();
        } catch (SQLException e)
        {
            e.printStackTrace();
            return null;
        }
    }
   
    
    public static String getDatabaseHost()
    {
        return DATABASE_HOST;
    }

    public static String getDatabaseUser()
    {
        return DATABASE_USER;
    }

    public static String getDatabasePass()
    {
        return DATABASE_PASS;
    }

    public static String getDatabaseName()
    {
        return DATABASE_NAME;
    }
}
