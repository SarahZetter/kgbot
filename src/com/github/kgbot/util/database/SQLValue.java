package com.github.kgbot.util.database;


public class SQLValue
{
    public SQLValue(String value)
    {
        this.value = value;
        datatype = "string";
    }
    
    public SQLValue(int value)
    {
        this.value = value;
        datatype = "integer";
    }
    
    public SQLValue(double value)
    {
        this.value = value;
        datatype = "double";
    }
    
    public SQLValue(boolean value)
    {
        this.value = value;
        datatype = "boolean";
    }

    private Object value;
    private String datatype;
    
    public Object getValue()
    {
        return (datatype.equalsIgnoreCase("string") ? "'" + value + "'" : value);
    }
    
    public void setValue(Object value)
    {
        this.value = value;
    }
    
    public String getDatatype()
    {
        return datatype;
    }
    
    public void setDatatype(String datatype)
    {
        this.datatype = datatype;
    }
    
}
