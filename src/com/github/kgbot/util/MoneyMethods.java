package com.github.kgbot.util;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;

import com.github.kgbot.util.database.Database;
import com.github.kgbot.util.database.SQLValue;

public class MoneyMethods
{
    public static double getMoney(String user)
    {
        try
        {
            ResultSet rs = Database.executeQuery("SELECT money FROM " + Database.TABLE_USERS + " WHERE username = " + new SQLValue(user).getValue());  
            rs.first();
            double money = rs.getInt("money");
            
            return money;
        } catch (SQLException e)
        {
            e.printStackTrace();
            return 0;
        }
    }
    
    private static DecimalFormat formatter = new DecimalFormat("#,###,###.##");
    public static final String CURRENCY_NAME = "Vodka Bottles";
    
    public static void setMoney(String user, double value)
    {
        Database.execute("UPDATE " + Database.TABLE_USERS +  " SET money = " + value + " WHERE username = " + new SQLValue(user).getValue());
    }
    
    public static boolean subtractMoney(String user, double value)
    {
        if (hasMoney(user, value))
        {
            setMoney(user, getMoney(user) - value);
            return true;
        } else return false;
    }
    
    public static boolean sendMoney(String user, String user2, double value)
    {
        if (hasMoney(user, value))
        {
            setMoney(user, getMoney(user) - value);
            setMoney(user2, getMoney(user2) + value);
            return true;
        } else return false;
    }
    
    public static void addMoney(String user, double value)
    {
        setMoney(user, getMoney(user) + value);
    }
    
    public static boolean hasMoney(String user, double value)
    {
        return getMoney(user) >= value;
    }

    public static String format(double amount)
    {
        return "&3" + formatter.format(amount) + " " + CURRENCY_NAME + "&n";
    }

    public static String getRichestUser()
    {
        try {
            ResultSet rs = Database.executeQuery("SELECT username FROM " + Database.TABLE_USERS + " ORDER BY money DESC");
            rs.first();
            return rs.getString("username");
        } catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }
    
    public static String getPoorestUser()
    {
        try {
            ResultSet rs = Database.executeQuery("SELECT username FROM " + Database.TABLE_USERS + " ORDER BY money ASC");
            rs.first();
            return rs.getString("username");
        } catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }
}