package com.github.kgbot.util.cron;

import java.util.Date;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import org.pircbotx.User;

import com.github.kgbot.main.KGBot;
import com.github.kgbot.util.MoneyMethods;
import com.github.kgbot.util.NumberMethods;
import com.github.kgbot.util.Users;
import com.github.kgbot.util.enums.ColorFormat;
import com.github.kgbot.util.messages.Messages;

public class Steal
{
    public static final long STEAL_MINUTES = 180;
    private static boolean initialized = false;
    
    private static class StealMoney
    {
        private static class LoopTask extends TimerTask
        {
            @Override
            public void run()
            {
                
                Set<User> users = KGBot.getBot().getUserChannelDao().getUsers(KGBot.getMainChannel());
                User thief = randomFromSet(users, null);
                User victim = randomFromSet(users, thief);
                if (thief != null && victim != null)
                {
                    double amount = NumberMethods.roll(1, (int) MoneyMethods.getMoney(victim.getNick()));
                    
                    MoneyMethods.sendMoney(victim.getNick(), thief.getNick(), amount);
                    Messages.sendMessage(KGBot.getMainChannel(), ColorFormat.NORMAL, thief.getNick() + " just stole " + MoneyMethods.format(amount) + " from " + victim.getNick() + "!");
                }
            }
        }
    
        long delay = 1000 * 60 * STEAL_MINUTES; 
        LoopTask task = new LoopTask();
        Timer timer = new Timer("StealMoney");
    
        public void start()
        {
            timer.cancel();
            timer = new Timer("StealMoney");
            Date executionDate = new Date(); // no params = now
            timer.scheduleAtFixedRate(task, executionDate, delay);
        }
    }
    
    public static User randomFromSet(Set<User> set, User not)
    {
        int i = 0;
        int random = NumberMethods.roll(0, set.size() - 1);
        
        for (User element : set)
        {
            if (i == random)
            {
                if (element != not && Users.isUser(element.getNick()))
                {
                    return element;
                }
            }
            i++;
        }
        return null;
    }
    
    public static void initialize()
    {
        if (!initialized)
        {
            StealMoney sm = new StealMoney();
            sm.start();
            initialized = true;
        }
    }
}
