package com.github.kgbot.util.cron;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Timer;
import java.util.TimerTask;

import org.pircbotx.Channel;

import com.github.kgbot.listeners.commands.Poll;
import com.github.kgbot.util.enums.ColorFormat;
import com.github.kgbot.util.messages.Messages;

public class ActualPoll
{    
    private class LoopTask extends TimerTask
    {
        @Override
        public void run()
        {
            Messages.sendMessage(channel, ColorFormat.POLL, "Poll ID &b" + id + "&n has ended! \"" + question + "\". Results:");
            int totalVotes = 0;            
            HashMap<Integer, Integer> votes = new HashMap<>();

            for (Entry<Integer, String> choice : choices.entrySet())
            {
                votes.put(choice.getKey(), 0);
            }
            
            for (Entry<String, Integer> choice : userVotes.entrySet())
            {
                votes.put(choice.getValue(), votes.get(choice.getValue()) + 1);
                totalVotes += 1;
            }
                 
            ArrayList<String> winners = new ArrayList<>();
            int winningVotes = 0;
            for (Entry<Integer, Integer> vote : votes.entrySet())
            {
                String choice = choices.get(vote.getKey());
                if (vote.getValue() > winningVotes)
                {
                    winningVotes = vote.getValue();
                    winners.clear();
                    winners.add(choice);
                } else if (vote.getValue() == winningVotes)
                {
                    winners.add(choice);
                }
                
                int percentage = totalVotes != 0 ? Math.round((vote.getValue() / totalVotes) * 100) : 0;
                Messages.sendMessage(channel, ColorFormat.POLL, "[" + vote.getKey() + "] " + choice + ": &b" + percentage + "%");
            }
            
            if (winners.size() > 1)
            {
                String winnersString = "";
                for (String winner : winners)
                {
                    winnersString += winner + ", ";
                }
                winnersString = winnersString.substring(0, winnersString.length() - 2);
                
                Messages.sendMessage(channel, ColorFormat.POLL, "&b" + winnersString + "&n are the tied winners with &b" + winningVotes + "&n votes!");
            } else Messages.sendMessage(channel, ColorFormat.POLL, "&b" + winners.get(0) + "&n is the winner with &b" + winningVotes + "&n votes!");
            
            Poll.getPolls().remove(id);
            Poll.getActualPolls().remove(id);
        }
    }
    
    public ActualPoll(int id, long duration, String question, String choices, Channel channel)
    {
        this.id = id;
        this.duration = duration;
        this.question = question;
        this.channel = channel;
        this.timer = new Timer("Poll" + String.valueOf(id));
        String[] splitChoices = choices.split(" ");
        
        for (int i = 0; i < splitChoices.length; i++)
        {
            this.choices.put(i, splitChoices[i]);
        }
        
        start();
    }
    
    private int id = 0;
    private long duration = 0;
    private HashMap<Integer, String> choices = new HashMap<>();
    private HashMap<String, Integer> userVotes = new HashMap<>();
    private String question = "";
    private Channel channel = null;
    private Timer timer;
    private LoopTask task = new LoopTask();
    
    public void start()
    {
        timer.schedule(task, duration * 60 * 1000);
    }
    
    public int getId()
    {
        return id;
    }

    public long getDuration()
    {
        return duration;
    }

    public HashMap<Integer, String> getChoices()
    {
        return choices;
    }

    public String getQuestion()
    {
        return question;
    }

    public Channel getChannel()
    {
        return channel;
    }

    public void vote(String user, int choiceId)
    {
        userVotes.put(user, choiceId);
    }
}
