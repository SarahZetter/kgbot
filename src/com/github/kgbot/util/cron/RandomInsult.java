package com.github.kgbot.util.cron;

import java.sql.ResultSet;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;

import com.github.kgbot.util.database.Database;
import com.github.kgbot.util.database.SQLValue;

public class RandomInsult
{
    public static final double INSULT_CHANCE = 0.01;
    private static HashMap<Integer, String> insults = new HashMap<Integer, String>();
    private static boolean initialized;

    public static int addInsult(String insult)
    {
        int id = insults.size() + 1;
        insults.put(id, insult);
        Database.execute("INSERT INTO " + Database.TABLE_INSULTS + " (id, insult) VALUES (" + id + ", " + new SQLValue(insult).getValue() + ")");
        return id;
    }


    public static String displayRandomQuote()
    {
        return null;

    }

    public static void editInsult(int oldInsultId, String newInsult)
    {
        Database.execute("UPDATE " + Database.TABLE_INSULTS + " SET insult = " + new SQLValue(newInsult).getValue() + " WHERE id = " + oldInsultId);
        insults.put(oldInsultId, newInsult);
    }

    public static HashMap<Integer, String> getInsults()
    {
        return insults;
    }
    
    public static String getRandomInsult()
    {
        Random random = new Random(new Date().getTime());
        int randInt = random.nextInt(insults.size());
        String insult = insults.get(randInt + 1);
        
        return insult;
    }

    public static void removeInsult(int insultId)
    {
        Database.execute("DELETE FROM " + Database.TABLE_INSULTS + " WHERE id = " + insultId);
        insults.remove(insultId);
    }
    
    public static void initialize()
    {
        if (!initialized)
        {
            try {
                ResultSet rs = Database.executeQuery("SELECT insult,id FROM " + Database.TABLE_INSULTS);
                
                insults.clear();
                while (rs.next())
                {
                    insults.put(rs.getInt("id"), rs.getString("insult"));   
                }
                
                initialized = true;
            } catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }
}
