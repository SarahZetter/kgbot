package com.github.kgbot.util;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map.Entry;

import com.github.kgbot.util.database.Database;

public class UserInventory
{
    private String owner;
    private HashMap<Integer, Integer> inventory = new HashMap<Integer, Integer>();
    
    public static UserInventory getInventory(String username)
    {
        try {
            if (Users.isUser(username))
            {
                UserInventory inventory = new UserInventory();
                inventory.setOwner(username);

                ResultSet rs = Database.executeQuery("SELECT inventory FROM " + Database.TABLE_USERS + " WHERE username = '" + username + "'");
                rs.first();
                
                String[] splitInventory = rs.getString("inventory").split(",");
                
                if (!splitInventory[0].equals(""))
                {
                    for (String item : splitInventory)
                    {
                        String[] splitItem = item.split("-");
                        Integer id = Integer.valueOf(splitItem[0]);
                        Integer amount = Integer.valueOf(splitItem[1]);
                        inventory.addItem(id, amount);
                    }
                }
                
                return inventory;
            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }

    public String getOwner()
    {
        return owner;
    }

    public void setOwner(String owner)
    {
        this.owner = owner;
    }

    public void addItem(Integer id, Integer amount)
    {
        Integer alreadyExisting = inventory.get(id) != null ? inventory.get(id) : 0;
        inventory.put(id, alreadyExisting + amount);
    }
    
    public void removeItem(Integer id, Integer amount)
    {
        Integer alreadyExisting = inventory.get(id) != null ? inventory.get(id) : 0;
        inventory.put(id, alreadyExisting - amount);
    }
    
    public void rewriteInventory()
    {
        String inventoryString = "";
        
        for (Entry<Integer, Integer> entry : inventory.entrySet())
        {
            inventoryString += entry.getKey() + "-" + entry.getValue() + ",";
        }
                
        Database.execute("UPDATE " + Database.TABLE_USERS + " SET inventory = '" + inventoryString + "' WHERE username = '" + owner + "'");
    }

    public HashMap<Integer, Integer> getInventory()
    {
        return inventory;
    }
}
