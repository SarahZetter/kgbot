package com.github.kgbot.util;

import java.util.HashMap;

public class Items
{
    public static HashMap<Integer, String> itemNames = new HashMap<Integer, String>();
    static {
        itemNames.put(0, "Shotgun");
        itemNames.put(1, "MP5");
        itemNames.put(2, "Burger");
        itemNames.put(3, "Vest");
    }
    
    public static HashMap<Integer, Integer> itemCosts = new HashMap<Integer, Integer>();
    static {
        itemCosts.put(0, 1500);
        itemCosts.put(1, 3000);
        itemCosts.put(2, 50);
        itemCosts.put(3, 5000);
    }
    
    public static String idToItem(Integer id)
    {
        return itemNames.get(id) != null ? itemNames.get(id) : "Error";
    }

    public static boolean isItem(int id)
    {
        return itemNames.get(id) != null ? true : false;
    }
}
