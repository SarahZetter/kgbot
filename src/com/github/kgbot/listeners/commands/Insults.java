package com.github.kgbot.listeners.commands;

import org.pircbotx.PircBotX;
import org.pircbotx.hooks.events.MessageEvent;

import com.github.kgbot.listeners.commands.core.BotCommand;
import com.github.kgbot.util.NumberMethods;
import com.github.kgbot.util.cron.RandomInsult;
import com.github.kgbot.util.enums.ColorFormat;
import com.github.kgbot.util.messages.ErrorMessages;
import com.github.kgbot.util.messages.Messages;

public class Insults extends BotCommand
{
    public Insults()
    {
        getAliases().add("!insults");
        getAliases().add("!insult");

        setDescription("Manages insults");

        setArgumentsString("(help|add|remove|edit|) (arguments)");
    }

    @Override
    public void onMessage(MessageEvent<PircBotX> event) throws Exception
    {
        if (performGenericChecks(event.getChannel(), event.getUser(), event.getMessage().split(" ")))
        {
            if (getArgs().length == 1)
            {
                Messages.sendMessage(getChannel(), ColorFormat.NORMAL, "For a list of commands, type &b!insults help");
            } else if (getArgs().length == 2)
            {
                switch (getArgs()[1].toLowerCase())
                {
                    case "help":
                        Messages.sendMessage(getChannel(), ColorFormat.NORMAL, "&b!insults help &n- Displays this text");
                        Messages.sendMessage(getChannel(), ColorFormat.NORMAL, "&b!insults add <text>&n- Adds an insult");
                        Messages.sendMessage(getChannel(), ColorFormat.NORMAL, "&b!insults remove <text>&n- Removes an insult");
                        Messages.sendMessage(getChannel(), ColorFormat.NORMAL, "&b!insults edit <insult id> <text> &n- Edits an insult");
                        break;
                }
            } else if (getArgs().length >= 3 && getArgs()[1].equalsIgnoreCase("add"))
            {
                String insult = "";
                for (int i = 2; i < getArgs().length; i++)
                {
                    insult += getArgs()[i] + " ";
                }
                insult = insult.substring(0, insult.length() - 1);
                if (!RandomInsult.getInsults().containsValue(insult))
                {
                    int insultId = RandomInsult.addInsult(insult.replace("'", ""));
                    Messages.respond(getChannel(), ColorFormat.NORMAL, getUser(), "Added insult &b[" + (insultId) + "] '" + insult + "'");
                } else ErrorMessages.insultAlreadyExists(getChannel(), getUser());
                return;
            } else if (getArgs().length >= 4 && getArgs()[1].equalsIgnoreCase("edit"))
            {
                int insultId = NumberMethods.isInteger(getArgs()[2]) ? Integer.valueOf(getArgs()[2]) : -1;
                if (insultId >= 0)
                {
                    String insult = RandomInsult.getInsults().get(insultId);
                    if (insult != null)
                    {
                        String newInsult = "";
                        for (int i = 3; i < getArgs().length; i++)
                        {
                            newInsult += getArgs()[i] + " ";
                        }
                        newInsult = newInsult.substring(0, newInsult.length() - 1);
                        RandomInsult.editInsult(insultId, newInsult.replace("'", ""));
                        Messages.respond(getChannel(), ColorFormat.NORMAL, getUser(), "Edited insult &b[" + (insultId) + "] '" + newInsult + "'");
                    } else ErrorMessages.insultDoesntExist(getChannel(), getUser());
                } else ErrorMessages.invalidNumber(getChannel(), getUser());
            } else if (getArgs().length == 3)
            {
                switch (getArgs()[1].toLowerCase())
                {
                    case "remove": case "delete":
                    {
                        int insultId = NumberMethods.isInteger(getArgs()[2]) ? Integer.valueOf(getArgs()[2]) : -1;
                        if (insultId >= 0)
                        {
                            if (insultId <= RandomInsult.getInsults().size())
                            {
                                String insult = RandomInsult.getInsults().get(insultId);
                                if (insult != null)
                                {
                                    RandomInsult.removeInsult(insultId);
                                    Messages.respond(getChannel(), ColorFormat.NORMAL, getUser(), "Removed insult &b[" + (insultId) + "] '" + insult + "'");
                                } else ErrorMessages.insultDoesntExist(getChannel(), getUser());
                            } else ErrorMessages.notEnoughInsults(getChannel(), getUser());
                        } else ErrorMessages.invalidNumber(getChannel(), getUser());
                        break;
                    }
                    case "view":
                    {
                        int insultId = NumberMethods.isInteger(getArgs()[2]) ? Integer.valueOf(getArgs()[2]) : -1;
                        if (insultId >= 0)
                        {
                            if (insultId <= RandomInsult.getInsults().size())
                            {
                                String insult = RandomInsult.getInsults().get(insultId);
                                if (insult != null)
                                {
                                    Messages.sendMessage(getChannel(), ColorFormat.INSULT, "&b[" + (insultId) + "]&n '" + insult + "'");
                                } else ErrorMessages.insultDoesntExist(getChannel(), getUser());
                            } else ErrorMessages.notEnoughInsults(getChannel(), getUser());
                        } else ErrorMessages.invalidNumber(getChannel(), getUser());
                        break;
                    }
                    default:
                        ErrorMessages.invalidInsultUsage(getChannel(), getUser());
                }
            } else showUsage();
        }
    }
}
