package com.github.kgbot.listeners.commands;

import java.util.Map.Entry;

import org.pircbotx.PircBotX;
import org.pircbotx.hooks.events.MessageEvent;

import com.github.kgbot.listeners.commands.core.BotCommand;
import com.github.kgbot.listeners.commands.core.BotUser;
import com.github.kgbot.util.Items;
import com.github.kgbot.util.MoneyMethods;
import com.github.kgbot.util.NumberMethods;
import com.github.kgbot.util.UserInventory;
import com.github.kgbot.util.enums.ColorFormat;
import com.github.kgbot.util.messages.ErrorMessages;
import com.github.kgbot.util.messages.Messages;

public class Shop extends BotCommand
{
    public Shop()
    {
        getAliases().add("!shop");
        
        setDescription("Controls thes shop");

        setArgumentsString("buy <id> <amount> | view");
    }

    public void view(BotUser user)
    {
        Messages.sendNotice(ColorFormat.NORMAL, user, "Welcome to the KGB shop!");
        
        for (Entry<Integer, Integer> entry : Items.itemCosts.entrySet())
        {
            Messages.sendNotice(ColorFormat.NORMAL, user, entry.getKey() + ". " + Items.idToItem(entry.getKey()) + " - " + MoneyMethods.format(entry.getValue()) + " each");
        }
        
        Messages.sendNotice(ColorFormat.NORMAL, user, "Buy with !shop buy <id> <amount>");
    }
    
    public void buy(BotUser user, String[] args) throws Exception
    {
        if (args.length == 4)
        {
            if (NumberMethods.isInteger(args[2]) && NumberMethods.isInteger(args[3]))
            {
                int id = Integer.valueOf(args[2]);
                int amount = Integer.valueOf(args[3]);
                double cost = Items.itemCosts.get(id);
                double total = cost * amount;

                if (amount > 0)
                {
                    if (Items.isItem(id))
                    {
                        if (MoneyMethods.subtractMoney(user.getNick(), total))
                        {
                            UserInventory inventory = UserInventory.getInventory(user.getNick());
                            inventory.addItem(id, amount);
                            inventory.rewriteInventory();
                           
                            Messages.respond(getChannel(), ColorFormat.NORMAL, user, MoneyMethods.format(total) + " have been taken from your account, in return for &3" + amount + "x " + Items.idToItem(id));
                        } else ErrorMessages.notEnoughMoney(getChannel(), user);
                    } else ErrorMessages.invalidItemId(getChannel(), getUser());
                } else ErrorMessages.invalidNumber(getChannel(), getUser());
            } else ErrorMessages.invalidNumber(getChannel(), getUser());
        } else showUsage();
    }
    
    @Override
    public void onMessage(MessageEvent<PircBotX> event) throws Exception
    {
        if (performGenericChecks(event.getChannel(), event.getUser(), event.getMessage().split(" ")))
        {
            if (getArgs().length >= 2)
            {
                switch (getArgs()[1].toLowerCase())
                {
                    case "view": case "items":
                        view(getUser());
                        break;
                    case "buy":
                        buy(getUser(), getArgs());
                        break;
                    default: 
                        showUsage();
                        break;
                }
            } else
            {
                view(getUser());
            }
        }
    }
}
