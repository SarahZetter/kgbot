package com.github.kgbot.listeners.commands;

import org.pircbotx.PircBotX;
import org.pircbotx.hooks.events.MessageEvent;

import com.github.kgbot.listeners.commands.core.BotCommand;
import com.github.kgbot.util.enums.ColorFormat;
import com.github.kgbot.util.enums.Link;
import com.github.kgbot.util.messages.ErrorMessages;
import com.github.kgbot.util.messages.Messages;

public class Links extends BotCommand
{
    public Links()
    {
        getAliases().add("!link");
        getAliases().add("!links");
        getAliases().add("!gimmelink");
        getAliases().add("!linkme");

        setDescription("Shows a list of useful links");

        setArgumentsString("");
    }

    @Override
    public void onMessage(MessageEvent<PircBotX> event) throws Exception
    {
        if (performGenericChecks(event.getChannel(), event.getUser(), event.getMessage().split(" ")))
        {
            if (getArgs().length == 1)
            {
                for (Link link : Link.values())
                {
                    Messages.sendMessage(getChannel(), ColorFormat.NORMAL, link.toString());
                }
                Messages.sendMessage(getChannel(), ColorFormat.NORMAL, "Use !links <link short name> next time to get the link you want! The short name is (in brackets)");
            } else if (getArgs().length == 2)
            {
                Link link = Link.getByShortName(getArgs()[1]);
                if (link != null)
                {
                    Messages.sendMessage(getChannel(), ColorFormat.NORMAL, link.toString());
                } else ErrorMessages.notLink(getChannel(), getUser(), getArgs()[1]);
            } else showUsage();
        }
    }
}
