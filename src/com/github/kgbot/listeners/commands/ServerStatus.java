package com.github.kgbot.listeners.commands;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.pircbotx.PircBotX;
import org.pircbotx.hooks.events.MessageEvent;

import com.github.kgbot.listeners.commands.core.BotCommand;
import com.github.kgbot.util.FileMethods;
import com.github.kgbot.util.NumberMethods;
import com.github.kgbot.util.enums.ColorFormat;
import com.github.kgbot.util.messages.ErrorMessages;
import com.github.kgbot.util.messages.Messages;

public class ServerStatus extends BotCommand
{
    public ServerStatus()
    {
        getAliases().add("!serverstatus");
        getAliases().add("!server");
        getAliases().add("!ss");

        setDescription("Displays server CPU usage and other nerdy shit");

        setArgumentsString("(server ID)");
    }
    
    public String displayServerStatus(String url)
    {
        ArrayList<String> contents = FileMethods.readFileFromURL(url);
        
        if (contents != null)
        {
            String line = contents.get(contents.size() - 1);
            
            //  0   0 100   0   0   0| 184B    0 
            Pattern pattern = Pattern.compile(" *[0-9]* *[0-9]* *([0-9]*) *[0-9]* *[0-9]* *[0-9]*\\| *([0-9]*.) *([0-9]*.).*");
            Matcher matcher = pattern.matcher(line);
            
            if (matcher.find())
            {
                String cpu = (100 - Integer.valueOf(matcher.group(1))) + "%";
                String recv = matcher.group(2).replace(" ", "");
                String send = matcher.group(3).replace(" ", "");
                
                if (recv.equalsIgnoreCase("0")) recv += "B";
                if (send.equalsIgnoreCase("0")) send += "B";
                
                return "CPU Usage &3" + cpu + "&n - Network Usage: SEND &3" + send + "&n - RECEIVE &3" + recv; 
            }
        }
        
        return "Error - Server may be off";
    }
    
    public String displayServer1()
    {
        return "Server 1 status: " + displayServerStatus("http://kurtmclester.com/stats/dstat.txt");
    }
    
    public String displayServer2()
    {
        return "Server 2 status: " + displayServerStatus("http://s2.kurtmclester.com/stats/dstat.txt");
    }

    @Override
    public void onMessage(MessageEvent<PircBotX> event) throws Exception
    {
        if (performGenericChecks(event.getChannel(), event.getUser(), event.getMessage().split(" ")))
        {
            if (getArgs().length == 1)
            {
                Messages.respond(getChannel(), ColorFormat.NORMAL, getUser(), displayServer1());
                Messages.respond(getChannel(), ColorFormat.NORMAL, getUser(), displayServer2());
            } else if (getArgs().length == 2)
            {
                if (NumberMethods.isInteger(getArgs()[1]))
                {
                    int id = Integer.valueOf(getArgs()[1]);
                    switch (id)
                    {
                        case 1:
                            Messages.respond(getChannel(), ColorFormat.NORMAL, getUser(), displayServer1());
                            break;
                        case 2:
                            Messages.respond(getChannel(), ColorFormat.NORMAL, getUser(), displayServer2());
                            break;
                        default:
                            ErrorMessages.invalidNumber(getChannel(), getUser());
                            break;
                    }
                } else ErrorMessages.invalidNumber(getChannel(), getUser());
            } else showUsage();
        }
    }
}
