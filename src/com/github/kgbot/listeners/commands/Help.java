package com.github.kgbot.listeners.commands;

import org.pircbotx.PircBotX;
import org.pircbotx.hooks.events.MessageEvent;

import com.github.kgbot.listeners.commands.core.BotCommand;
import com.github.kgbot.main.KGBot;
import com.github.kgbot.util.enums.ColorFormat;
import com.github.kgbot.util.messages.ErrorMessages;
import com.github.kgbot.util.messages.Messages;

public class Help extends BotCommand
{
    public Help()
    {
        getAliases().add("!help");
        getAliases().add("!commands");
        getAliases().add("!confused");
        getAliases().add("!derp");
        getAliases().add("!halp");

        setDescription("Shows this message");

        setArgumentsString("");
    }

    @Override
    public void onMessage(MessageEvent<PircBotX> event) throws Exception
    {
        if (performGenericChecks(event.getChannel(), event.getUser(), event.getMessage().split(" ")))
        {
            if (getArgs().length == 1)
            {
                Messages.sendNotice(ColorFormat.NORMAL, getUser(), "&uKGBot help: The ultimate guide for absolute retards");
                Messages.sendNotice(ColorFormat.NORMAL, getUser(), "Commands:");

                for (BotCommand command : KGBot.getCommands())
                {
                    String commandHelp = "!" + command.getAliasesString() + " " + command.getArgumentsString() + " - " + command.getDescription();
                    Messages.sendNotice(ColorFormat.NORMAL, getUser(), commandHelp);
                }

            } else if (getArgs().length == 2)
            {
                String argument = getArgs()[1];
                BotCommand command = argument.startsWith("!") ? BotCommand.getCommand(argument) : BotCommand.getCommand(argument.replace(argument, "!" + argument));

                if (command != null)
                {
                    Messages.sendNotice(ColorFormat.NORMAL, getUser(), "!" + command.getAliasesString() + " " + command.getArgumentsString() + " - " + command.getDescription());
                } else ErrorMessages.notCommand(getChannel(), getUser(), getArgs()[1]);
            } else showUsage();
        }
    }
}