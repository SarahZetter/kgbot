package com.github.kgbot.listeners.commands.core;

import java.util.ArrayList;
import java.util.Arrays;

import org.pircbotx.Channel;
import org.pircbotx.PircBotX;
import org.pircbotx.User;
import org.pircbotx.hooks.ListenerAdapter;
import org.pircbotx.hooks.events.MessageEvent;

import com.github.kgbot.main.KGBot;
import com.github.kgbot.util.enums.ColorFormat;
import com.github.kgbot.util.messages.Messages;

public abstract class BotCommand extends ListenerAdapter<PircBotX>
{
    private ArrayList<String> aliases = new ArrayList<String>();
    private String[] args;
    private BotUser user;
    private String description;
    private Channel channel;
    
    public Channel getChannel()
    {
        return channel;
    }

    public void setChannel(Channel channel)
    {
        this.channel = channel;
    }

    private String argumentsString;
    private boolean worksWhenDeactivated;

    public static BotCommand getCommand(String command)
    {
        for (BotCommand botCommand : KGBot.getCommands())
        {
            if (botCommand.isAlias(command)) return botCommand;
        }
        return null;
    }

    public ArrayList<String> getAliases()
    {
        return aliases;
    }

    public String getAliasesString()
    {
        String returnValue = "";

        for (String alias : aliases)
        {
            returnValue += alias.replace("!", "") + "&15|&n";
        }

        return returnValue.substring(0, returnValue.length() - 6);
    }

    public String[] getArgs()
    {
        return args;
    }

    public String getArgumentsString()
    {
        return "&b&14" + argumentsString + "&n";
    }

    public String getDescription()
    {
        return "&7" + description + "&n";
    }

    public BotUser getUser()
    {
        return user;
    }

    public boolean isAlias(String alias)
    {
        for (String temp : aliases)
            if (temp.equalsIgnoreCase(alias)) return true;
        return false;
    }

    public boolean isWorksWhenDeactivated()
    {
        return worksWhenDeactivated;
    }

    @Override
    public abstract void onMessage(MessageEvent<PircBotX> event) throws Exception;

    public boolean performGenericChecks(Channel channel, User user, String args[])
    {
        setChannel(channel);
        setUser(new BotUser(user));
        setArgs(args);
        
        if (KGBot.isActivated() || worksWhenDeactivated)
        {            
            if (args[0].startsWith("!"))
            {
                if (isAlias(args[0])) {
                    return true;
                }
            } else if (user.getNick().equalsIgnoreCase("KGB-KGBot")) {
                if (args[1].startsWith("!") && isAlias(args[1])) {
                    String skypeUser = args[0].replace(">", "").replace("<", "");
                    setUser(new BotUser(skypeUser));
                    System.out.println(Arrays.toString(args));
                    String[] newArgs = new String[args.length - 1];
                    System.arraycopy(args, 1, newArgs, 0, args.length - 1);
                    setArgs(newArgs);
                    System.out.println(Arrays.toString(args));
                    return true;
                }
            }
        }
        return false;
    }

    public void setAliases(ArrayList<String> aliases)
    {
        this.aliases = aliases;
    }

    public void setArgs(String[] args)
    {
        this.args = args;
    }

    public void setArgumentsString(String argumentsString)
    {
        this.argumentsString = argumentsString;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }
    
    public void setUser(BotUser user)
    {
        this.user = user;
    }

    public void setWorksWhenDeactivated(boolean worksWhenDeactivated)
    {
        this.worksWhenDeactivated = worksWhenDeactivated;
    }

    public void showUsage() throws Exception
    {
        Messages.sendMessage(getChannel(), ColorFormat.NORMAL, user.getNick() + ": " + getAliasesString() + " " + getArgumentsString());
    }

}
