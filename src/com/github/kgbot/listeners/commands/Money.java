package com.github.kgbot.listeners.commands;

import org.pircbotx.PircBotX;
import org.pircbotx.hooks.events.MessageEvent;

import com.github.kgbot.listeners.commands.core.BotCommand;
import com.github.kgbot.util.MoneyMethods;
import com.github.kgbot.util.NumberMethods;
import com.github.kgbot.util.Users;
import com.github.kgbot.util.enums.ColorFormat;
import com.github.kgbot.util.messages.ErrorMessages;
import com.github.kgbot.util.messages.Messages;

public class Money extends BotCommand
{
    public Money()
    {
        getAliases().add("!money");
        getAliases().add("!moolah");
        getAliases().add("!rootofallevil");
        
        setDescription("Controls money");

        setArgumentsString("richest|balance|send <player> <amount>");
    }

    @Override
    public void onMessage(MessageEvent<PircBotX> event) throws Exception
    {
        if (performGenericChecks(event.getChannel(), event.getUser(), event.getMessage().split(" ")))
        {
            if (getArgs().length >= 2)
            {
                switch (getArgs()[1].toLowerCase())
                {
                    case "poorest":
                        if (getArgs().length == 2)
                        {
                            String poorest = MoneyMethods.getPoorestUser();
                            Messages.respond(getChannel(), ColorFormat.NORMAL, getUser(), "The poorest user is &b" + poorest + "&n with &b" + MoneyMethods.format(MoneyMethods.getMoney(poorest)) + "&n! WHATAFAG");
                        } else showUsage();
                        break;
                    case "richest":
                        if (getArgs().length == 2)
                        {
                            String richest = MoneyMethods.getRichestUser();
                            Messages.respond(getChannel(), ColorFormat.NORMAL, getUser(), "The richest user is &b" + richest + "&n with &b" + MoneyMethods.format(MoneyMethods.getMoney(richest)) + "&n! WHATABOSS");
                        } else showUsage();
                        break;
                    case "send":
                        if (getArgs().length == 4)
                        {
                            if (Users.isUser(getUser().getNick()))
                            {
                                if (Users.isUser(getArgs()[2]))
                                {
                                    if (NumberMethods.isDouble(getArgs()[3]))
                                    {
                                        double amount = Double.parseDouble(getArgs()[3]);
                                        if (amount > 0)
                                        {
                                            if (MoneyMethods.sendMoney(getUser().getNick(), getArgs()[2], amount))
                                            {
                                                Messages.respond(getChannel(), ColorFormat.NORMAL, getUser(), "You have sent " + MoneyMethods.format(amount) + " to " + getArgs()[2] + ".");
                                            } else ErrorMessages.notEnoughMoney(getChannel(), getUser());
                                        } else ErrorMessages.invalidNumber(getChannel(), getUser());
                                    } else ErrorMessages.invalidNumber(getChannel(), getUser());
                                } else ErrorMessages.invalidUser(getChannel(), getUser(), getArgs()[2]);
                            } else ErrorMessages.invalidUser(getChannel(), getUser(), getUser().getNick());
                        } else showUsage();
                        break;
                    case "balance":
                        if (getArgs().length == 2)
                        {
                            if (Users.isUser(getUser().getNick()))
                            {
                                Messages.respond(getChannel(), ColorFormat.NORMAL, getUser(), "Your balance is &b" + MoneyMethods.format(MoneyMethods.getMoney(getUser().getNick())) + "&n.");
                            } else ErrorMessages.invalidUser(getChannel(), getUser(), getUser().getNick());
                        } else if (getArgs().length == 3)
                        {
                            if (Users.isUser(getArgs()[2]))
                            {
                                Messages.respond(getChannel(), ColorFormat.NORMAL, getUser(), getArgs()[2] + "'s balance is &b" + MoneyMethods.format(MoneyMethods.getMoney(getArgs()[2])) + "&n.");
                            } else ErrorMessages.invalidUser(getChannel(), getUser(), getArgs()[2]);
                        } else showUsage();
                        break;
                    case "help":
                        Messages.sendNotice(ColorFormat.NORMAL, getUser(), "!money commands list:");
                        Messages.sendNotice(ColorFormat.NORMAL, getUser(), "!money help: - Displays this message.");
                        Messages.sendNotice(ColorFormat.NORMAL, getUser(), "!money balance (player): - Displays your or another player's balance.");
                        Messages.sendNotice(ColorFormat.NORMAL, getUser(), "!money send <player> <amount>: - Sends an amount of money to another player.");
                        break;
                    default: 
                        showUsage();
                        break;
                }
            } else showUsage();
        }
    }
}
