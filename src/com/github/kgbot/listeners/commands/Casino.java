package com.github.kgbot.listeners.commands;

import java.util.ArrayList;
import java.util.HashMap;

import org.pircbotx.PircBotX;
import org.pircbotx.hooks.events.MessageEvent;

import com.github.kgbot.listeners.commands.core.BotCommand;
import com.github.kgbot.util.MoneyMethods;
import com.github.kgbot.util.NumberMethods;
import com.github.kgbot.util.Users;
import com.github.kgbot.util.enums.ColorFormat;
import com.github.kgbot.util.messages.ErrorMessages;
import com.github.kgbot.util.messages.Messages;

public class Casino extends BotCommand
{
    public Casino()
    {
        getAliases().add("!casino");
        getAliases().add("!gamble");

        setDescription("Gambles in the virtual casino");

        setArgumentsString("flip <user> <amount>|help|game <bet> (additional args)");
    }
    
    private static HashMap<String, String[]> flips = new HashMap<>();
    private static ArrayList<String> taunts = new ArrayList<>();
    
    static {
        taunts.add("<winner> just stole candy from a baby.");
        taunts.add("<loser>: Don't worry; HIS POWER LEVEL WAS OVER NYAN THOUSAND");
        taunts.add("<winner> WAS TOO STRONK, TOO STRONK TOO STRONK");
        taunts.add("FATALITY!");
        taunts.add("WHO THE OBERBOSS? <winner> THE OBERBOSS");
        taunts.add("<loser> WAS HUMILIATED");
        taunts.add("DAYLIGHT ROBBERY HAS NEVER BEEN MORE FUN");
        taunts.add("I'LL JUST TAKE YOUR MONEY, THANK YOU.");
        taunts.add("<loser>: TO BORI'S CELLAR WITH YOU");
        taunts.add("ONE DOES NOT SIMPLY WALK AWAY FROM THE BATTLE WITH HIS HONOUR!");
        taunts.add("ARE YOU PAYING TOO MUCH FOR YOUR CAR INSURANCE?");
    }
    
    public static String getRandomTaunt(String winner, String loser)
    {
        String taunt = taunts.get(NumberMethods.roll(0, taunts.size() - 1));
        return taunt.replace("<winner>", winner).replace("<loser>", loser);
    }

    @Override
    public void onMessage(MessageEvent<PircBotX> event) throws Exception
    {
        if (performGenericChecks(event.getChannel(), event.getUser(), event.getMessage().split(" ")))
        {
            if (getArgs().length >= 2)
            {
                switch (getArgs()[1].toLowerCase())
                {
                    case "acceptflip": case "af":
                        if (getArgs().length == 2)
                        {
                            String[] flipInfo = flips.get(getUser().getNick());
                            if (flipInfo != null)
                            {
                                double amount = Double.valueOf(flipInfo[1]);
                                if (MoneyMethods.hasMoney(getUser().getNick(), amount))
                                {
                                    if (MoneyMethods.hasMoney(flipInfo[0], amount))
                                    {
                                        Messages.sendMessage(getChannel(), ColorFormat.CASINO, "Rolling coinflip for " + getUser().getNick() + " and " + flipInfo[0] + " for " + MoneyMethods.format(Double.valueOf(flipInfo[1])) + "!");
                                        Messages.sendMessage(getChannel(), ColorFormat.CASINO, "1 = " + getUser().getNick() + " wins. 2 = " + flipInfo[0] + " wins.");
                                        
                                        int random = NumberMethods.roll(1, 2);
                                        
                                        Messages.sendMessage(getChannel(), ColorFormat.CASINO, random + " was rolled!");
                                        
                                        String winner = "";
                                        String loser = "";
                                        if (random == 1)
                                        {
                                            winner = getUser().getNick();
                                            loser = flipInfo[0];
                                        } else {
                                            winner = flipInfo[0];
                                            loser = getUser().getNick();
                                        }
                                        
                                        MoneyMethods.addMoney(winner, amount);
                                        MoneyMethods.subtractMoney(loser, amount);
                                        
                                        flips.remove(getUser().getNick());
                                                                            
                                        Messages.sendMessage(getChannel(), ColorFormat.CASINO, winner + " wins " + MoneyMethods.format(amount) + "! " + getRandomTaunt(winner, loser));
                                    } else ErrorMessages.notEnoughMoneyOther(getChannel(), getUser(), flipInfo[0]);
                                } else ErrorMessages.notEnoughMoney(getChannel(), getUser());
                            } else ErrorMessages.notChallengedToFlip(getChannel(), getUser());
                        } else showUsage();
                        break;
                    case "flip":
                        if (getArgs().length == 4)
                        {
                            if (Users.isUser(getUser().getNick()))
                            {
                                String other = getArgs()[2];
                                if (Users.isUser(other))
                                {
                                    if (NumberMethods.isDouble(getArgs()[3]))
                                    {
                                        double amount = Double.parseDouble(getArgs()[3]);
                                        if (amount > 0)
                                        {
                                            if (MoneyMethods.hasMoney(getUser().getNick(), amount))
                                            {
                                                flips.put(other, new String[] { getUser().getNick(), getArgs()[3] });
                                                Messages.sendMessage(getChannel(), ColorFormat.CASINO, getUser().getNick() + " has challenged " + other + " to a coin flip for " + MoneyMethods.format(amount) + "! Accept with !casino acceptflip.");
                                            } else ErrorMessages.notEnoughMoney(getChannel(), getUser());
                                        } else ErrorMessages.invalidNumber(getChannel(), getUser());
                                    } else ErrorMessages.invalidNumber(getChannel(), getUser());
                                } else ErrorMessages.invalidUser(getChannel(), getUser(), other);
                            } else ErrorMessages.invalidUser(getChannel(), getUser(), getUser().getNick());
                        } else showUsage();
                        break;
                    case "help":
                        Messages.sendNotice(ColorFormat.NORMAL, getUser(), "!casino commands list:");
                        Messages.sendNotice(ColorFormat.NORMAL, getUser(), "!casino help: - Displays this message.");
                        Messages.sendNotice(ColorFormat.NORMAL, getUser(), "!casino flip <player>: - Challenges another player to a coinflip.");
                        Messages.sendNotice(ColorFormat.NORMAL, getUser(), "!casino acceptflip|af: - Accepts the last challenge to a coinflip.");
                        break;
                    default: 
                        showUsage(); 
                        break;
                }
            } else showUsage();
        }
    }
}