package com.github.kgbot.listeners.commands;

import java.sql.ResultSet;
import java.util.ArrayList;

import org.pircbotx.PircBotX;
import org.pircbotx.User;
import org.pircbotx.hooks.events.MessageEvent;

import com.github.kgbot.listeners.commands.core.BotCommand;
import com.github.kgbot.main.KGBot;
import com.github.kgbot.util.database.Database;
import com.github.kgbot.util.database.SQLValue;
import com.github.kgbot.util.enums.ColorFormat;
import com.github.kgbot.util.messages.Messages;

public class Users extends BotCommand
{
    public Users()
    {
        getAliases().add("!users");
        getAliases().add("!faggots");

        setDescription("Controls users");

        setArgumentsString("import | help");
    }
    
    public static ArrayList<String> ignoreList = new ArrayList<>();
    static {
        ignoreList.add("KGBot");
        ignoreList.add("m15");
        ignoreList.add("Internets");
        ignoreList.add("Kazzy1");
    }

    @Override
    public void onMessage(MessageEvent<PircBotX> event) throws Exception
    {
        if (performGenericChecks(event.getChannel(), event.getUser(), event.getMessage().split(" ")))
        {
            if (getArgs().length >= 2)
            {
                switch (getArgs()[1])
                {
                    case "import":
                        String importList = "";
                        for (User user : KGBot.getBot().getUserChannelDao().getUsers(getChannel()))
                        {
                            if (!ignoreList.contains(user.getNick()))
                            {
                                ResultSet rs = Database.executeQuery("SELECT username FROM " + Database.TABLE_USERS + " WHERE username = " + new SQLValue(user.getNick()).getValue()); 
    
                                if (!rs.first())
                                {
                                    Database.execute("INSERT INTO " + Database.TABLE_USERS + " (username) VALUES (" +  new SQLValue(user.getNick()).getValue() + ")");
                                    importList += user.getNick() + ", ";
                                }
                            }
                        }
                        if (!importList.equalsIgnoreCase(""))
                        {
                            importList = importList.substring(0, importList.length() - 2);
                            Messages.respond(getChannel(), ColorFormat.NORMAL, getUser(), "Users imported: " + importList);
                        } else {
                            Messages.respond(getChannel(), ColorFormat.NORMAL, getUser(), "Database up to date already - all users imported.");
                        }
                        
                        break;
                    case "help": default: 
                        Messages.respond(getChannel(), ColorFormat.NORMAL, getUser(), "!users commands list:");
                        Messages.respond(getChannel(), ColorFormat.NORMAL, getUser(), "!users help: - Displays this message.");
                        Messages.respond(getChannel(), ColorFormat.NORMAL, getUser(), "!users import: - Imports all new users into the database.");
                        break;
                }
            } else showUsage();
        }
    }
}
