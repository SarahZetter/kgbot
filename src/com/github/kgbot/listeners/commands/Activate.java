package com.github.kgbot.listeners.commands;

import org.pircbotx.PircBotX;
import org.pircbotx.hooks.events.MessageEvent;

import com.github.kgbot.listeners.commands.core.BotCommand;
import com.github.kgbot.main.KGBot;
import com.github.kgbot.util.enums.ColorFormat;
import com.github.kgbot.util.messages.ErrorMessages;
import com.github.kgbot.util.messages.Messages;

public class Activate extends BotCommand
{
    public Activate()
    {
        getAliases().add("!activate");
        getAliases().add("!on");
        getAliases().add("!workffs");

        setDescription("Activates the bot");

        setArgumentsString("");

        setWorksWhenDeactivated(true);
    }

    @Override
    public void onMessage(MessageEvent<PircBotX> event) throws Exception
    {
        if (performGenericChecks(event.getChannel(), event.getUser(), event.getMessage().split(" ")))
        {
            if (getArgs().length == 1)
            {
                if (!KGBot.isActivated())
                {
                    Messages.sendMessage(getChannel(), ColorFormat.NORMAL, "Bot activated :)");
                    KGBot.setActivated(true);
                } else ErrorMessages.alreadyActivated(getChannel(), getUser());
            } else showUsage();
        }
    }
}
