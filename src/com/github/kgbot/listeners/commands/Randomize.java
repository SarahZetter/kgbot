package com.github.kgbot.listeners.commands;

import org.pircbotx.PircBotX;
import org.pircbotx.hooks.events.MessageEvent;

import com.github.kgbot.listeners.commands.core.BotCommand;
import com.github.kgbot.util.NumberMethods;
import com.github.kgbot.util.enums.ColorFormat;
import com.github.kgbot.util.messages.ErrorMessages;
import com.github.kgbot.util.messages.Messages;

public class Randomize extends BotCommand
{
    public Randomize()
    {
        getAliases().add("!random");
        getAliases().add("!randomize");
        getAliases().add("!dice");
        getAliases().add("!roll");

        setDescription("Rolls a random integer from min to max");

        setArgumentsString("(min) <max>");
    }

    @Override
    public void onMessage(MessageEvent<PircBotX> event) throws Exception
    {
        if (performGenericChecks(event.getChannel(), event.getUser(), event.getMessage().split(" ")))
        {
            int min = 0;
            int max = 0;

            if (getArgs().length == 3)
            {
                if (NumberMethods.isInteger(getArgs()[1]) && NumberMethods.isInteger(getArgs()[2]))
                {
                    min = Integer.valueOf(getArgs()[1]);
                    max = Integer.valueOf(getArgs()[2]);
                } else
                {
                    ErrorMessages.invalidNumber(getChannel(), getUser());
                    return;
                }
            } else if (getArgs().length == 2)
            {
                if (NumberMethods.isInteger(getArgs()[1]))
                {
                    min = 0;
                    max = Integer.valueOf(getArgs()[1]);
                } else
                {
                    ErrorMessages.invalidNumber(getChannel(), getUser());
                    return;
                }
            } else
            {
                showUsage();
                return;
            }
            
            int random = NumberMethods.roll(min, max);
            Messages.respond(getChannel(), ColorFormat.NORMAL, getUser(), "Rolled random number (" + min + " to " + max + "): " + random);
        }
    }
}
