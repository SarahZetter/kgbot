package com.github.kgbot.listeners.commands;

import org.pircbotx.PircBotX;
import org.pircbotx.hooks.events.MessageEvent;

import com.github.kgbot.listeners.commands.core.BotCommand;
import com.github.kgbot.util.NumberMethods;
import com.github.kgbot.util.cron.RandomQuote;
import com.github.kgbot.util.enums.ColorFormat;
import com.github.kgbot.util.enums.Link;
import com.github.kgbot.util.messages.ErrorMessages;
import com.github.kgbot.util.messages.Messages;

public class Quotes extends BotCommand
{
    public Quotes()
    {
        getAliases().add("!quotes");
        getAliases().add("!quote");
        getAliases().add("!shitpeoplesaid");

        setDescription("Manages quotes");

        setArgumentsString("(help|add|remove|edit|random|last|view) (arguments)");
    }

    @Override
    public void onMessage(MessageEvent<PircBotX> event) throws Exception
    {
        if (performGenericChecks(event.getChannel(), event.getUser(), event.getMessage().split(" ")))
        {
            if (getArgs().length == 1)
            {
                Messages.sendMessage(getChannel(), ColorFormat.NORMAL, "&uList of quotes:&n &b" + Link.QUOTES.getUrl());
                Messages.sendMessage(getChannel(), ColorFormat.NORMAL, "For a list of commands, type &b!quotes help");
            } else if (getArgs().length == 2)
            {
                switch (getArgs()[1].toLowerCase())
                {
                    case "help":
                        Messages.sendMessage(getChannel(), ColorFormat.NORMAL, "&b!quotes &n- Displays the link to the quotes list");
                        Messages.sendMessage(getChannel(), ColorFormat.NORMAL, "&b!quotes help &n- Displays this text");
                        Messages.sendMessage(getChannel(), ColorFormat.NORMAL, "&b!quotes add <text>&n- Adds a quote");
                        Messages.sendMessage(getChannel(), ColorFormat.NORMAL, "&b!quotes remove <text>&n- Removes a quote");
                        Messages.sendMessage(getChannel(), ColorFormat.NORMAL, "&b!quotes edit <quote id> <text> &n- Edits a quote");
                        Messages.sendMessage(getChannel(), ColorFormat.NORMAL, "&b!quotes random &n- Displays a random quote");
                        Messages.sendMessage(getChannel(), ColorFormat.NORMAL, "&b!quotes last &n- Displays the last quote");
                        Messages.sendMessage(getChannel(), ColorFormat.NORMAL, "&b!quotes view <quote id> &n- Displays the specified quote");
                        Messages.sendMessage(getChannel(), ColorFormat.NORMAL, "&b!quotes reload &n- Reloads all quotes");
                        Messages.sendMessage(getChannel(), ColorFormat.NORMAL, "&4&b<quote id>&n&4 represents the quote number in " + Link.QUOTES.getUrl() + ".");
                        break;
                    case "random":
                        RandomQuote.displayRandomQuote();
                        break;
                    case "last":
                        RandomQuote.displayLastQuote(getChannel());
                        break;
                    case "reload":
                        RandomQuote.initialize();
                        Messages.sendMessage(getChannel(), ColorFormat.NORMAL, "Reloaded all quotes!");
                        break;
                    default:
                        ErrorMessages.invalidQuoteUsage(getChannel(), getUser());
                }
            } else if (getArgs().length >= 3 && getArgs()[1].equalsIgnoreCase("add"))
            {
                String quote = "";
                for (int i = 2; i < getArgs().length; i++)
                {
                    quote += getArgs()[i] + " ";
                }
                quote = quote.substring(0, quote.length() - 1);
                if (!RandomQuote.getQuotes().containsValue(quote))
                {
                    int quoteId = RandomQuote.addQuote(quote.replace("'", ""));
                    Messages.respond(getChannel(), ColorFormat.NORMAL, getUser(), "Added quote &b[" + (quoteId) + "] '" + quote + "'");
                } else ErrorMessages.quoteAlreadyExists(getChannel(), getUser());
                return;
            } else if (getArgs().length >= 4 && getArgs()[1].equalsIgnoreCase("edit"))
            {
                int quoteId = NumberMethods.isInteger(getArgs()[2]) ? Integer.valueOf(getArgs()[2]) : -1;
                if (quoteId >= 0)
                {
                    String quote = RandomQuote.getQuotes().get(quoteId);
                    if (quote != null)
                    {
                        String newQuote = "";
                        for (int i = 3; i < getArgs().length; i++)
                        {
                            newQuote += getArgs()[i] + " ";
                        }
                        newQuote = newQuote.substring(0, newQuote.length() - 1);
                        RandomQuote.editQuote(quoteId, newQuote.replace("'", ""));
                        Messages.respond(getChannel(), ColorFormat.NORMAL, getUser(), "Edited quote &b[" + (quoteId) + "] '" + newQuote + "'");
                    } else ErrorMessages.quoteDoesntExist(getChannel(), getUser());
                } else ErrorMessages.invalidNumber(getChannel(), getUser());
            } else if (getArgs().length == 3)
            {
                switch (getArgs()[1].toLowerCase())
                {
                    case "remove": case "delete":
                    {
                        int quoteId = NumberMethods.isInteger(getArgs()[2]) ? Integer.valueOf(getArgs()[2]) : -1;
                        if (quoteId >= 0)
                        {
                            if (quoteId <= RandomQuote.getQuotes().size())
                            {
                                String quote = RandomQuote.getQuotes().get(quoteId);
                                if (quote != null)
                                {
                                    RandomQuote.removeQuote(quoteId);
                                    Messages.respond(getChannel(), ColorFormat.NORMAL, getUser(), "Removed quote &b[" + (quoteId) + "] '" + quote + "'");
                                } else ErrorMessages.quoteDoesntExist(getChannel(), getUser());
                            } else ErrorMessages.notEnoughQuotes(getChannel(), getUser());
                        } else ErrorMessages.invalidNumber(getChannel(), getUser());
                        break;
                    }
                    case "view":
                    {
                        int quoteId = NumberMethods.isInteger(getArgs()[2]) ? Integer.valueOf(getArgs()[2]) : -1;
                        if (quoteId >= 0)
                        {
                            if (quoteId <= RandomQuote.getQuotes().size())
                            {
                                String quote = RandomQuote.getQuotes().get(quoteId);
                                if (quote != null)
                                {
                                    Messages.sendMessage(getChannel(), ColorFormat.QUOTE, "&b[" + (quoteId) + "]&n '" + quote + "'");
                                } else ErrorMessages.quoteDoesntExist(getChannel(), getUser());
                            } else ErrorMessages.notEnoughQuotes(getChannel(), getUser());
                        } else ErrorMessages.invalidNumber(getChannel(), getUser());
                        break;
                    }
                    default:
                        ErrorMessages.invalidQuoteUsage(getChannel(), getUser());
                }
            } else showUsage();
        }
    }
}
