package com.github.kgbot.listeners.commands;

import org.pircbotx.PircBotX;
import org.pircbotx.hooks.events.MessageEvent;

import com.github.kgbot.listeners.commands.core.BotCommand;
import com.github.kgbot.util.NumberMethods;
import com.github.kgbot.util.enums.ColorFormat;
import com.github.kgbot.util.messages.ErrorMessages;
import com.github.kgbot.util.messages.Messages;

public class Calculate extends BotCommand
{
    public Calculate()
    {
        getAliases().add("!calculate");
        getAliases().add("!calc");

        setDescription("A simple calculator");

        setArgumentsString("<number1> <+-*/^%> <number2>|sqrt <number>");
    }

    @Override
    public void onMessage(MessageEvent<PircBotX> event) throws Exception
    {
        if (performGenericChecks(event.getChannel(), event.getUser(), event.getMessage().split(" ")))
        {
            if (getArgs().length == 4)
            {
                String operation = getArgs()[2];

                if (NumberMethods.isDouble(getArgs()[1]) && NumberMethods.isDouble(getArgs()[3]))
                {
                    double num1 = Double.valueOf(getArgs()[1]);
                    double num2 = Double.valueOf(getArgs()[3]);
                    switch (operation)
                    {
                        case "+":
                            Messages.respond(getChannel(), ColorFormat.NORMAL, getUser(), num1 + " + " + num2 + " = &b" + (num1 + num2));
                            break;
                        case "-":
                            Messages.respond(getChannel(), ColorFormat.NORMAL, getUser(), num1 + " - " + num2 + " = &b" + (num1 - num2));
                            break;
                        case "*":
                            Messages.respond(getChannel(), ColorFormat.NORMAL, getUser(), num1 + " * " + num2 + " = &b" + (num1 * num2));
                            break;
                        case "/":
                            Messages.respond(getChannel(), ColorFormat.NORMAL, getUser(), num1 + " / " + num2 + " = &b" + (num1 / num2));
                            break;
                        case "^":
                            Messages.respond(getChannel(), ColorFormat.NORMAL, getUser(), num1 + " ^ " + num2 + " = &b" + (Math.pow(num1, num2)));
                            break;
                        case "%":
                            Messages.respond(getChannel(), ColorFormat.NORMAL, getUser(), num1 + "% of " + num2 + " = &b" + (num2 / 100 * num1));
                            break;
                        default:
                            ErrorMessages.invalidOperation(getChannel(), getUser());
                            break;
                    }
                } else ErrorMessages.invalidNumber(getChannel(), getUser());
            } else if (getArgs().length == 3)
            {
                String operation = getArgs()[1];

                if (NumberMethods.isDouble(getArgs()[2]))
                {
                    double number = Double.valueOf(getArgs()[2]);
                    switch (operation)
                    {
                        case "sqrt":
                            Messages.respond(getChannel(), ColorFormat.NORMAL, getUser(), "The square root of " + number + " = &b" + Math.sqrt(number));
                            break;
                        default:
                            ErrorMessages.invalidOperation(getChannel(), getUser());
                            break;
                    }
                } else ErrorMessages.invalidNumber(getChannel(), getUser());
            } else showUsage();
        }
    }
}
