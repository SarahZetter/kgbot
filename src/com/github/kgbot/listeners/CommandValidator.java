package com.github.kgbot.listeners;

import org.pircbotx.PircBotX;
import org.pircbotx.hooks.ListenerAdapter;
import org.pircbotx.hooks.events.MessageEvent;

import com.github.kgbot.listeners.commands.core.BotCommand;
import com.github.kgbot.listeners.commands.core.BotUser;
import com.github.kgbot.main.KGBot;
import com.github.kgbot.util.messages.ErrorMessages;

public class CommandValidator extends ListenerAdapter<PircBotX>
{
    @Override
    public void onMessage(MessageEvent<PircBotX> event)
    {
        if (KGBot.isActivated())
        {
            if (event.getMessage().startsWith("!"))
            {
                String command = event.getMessage().split(" ")[0];
                if (BotCommand.getCommand(command) == null) ErrorMessages.notCommand(event.getChannel(), new BotUser(event.getUser()), command);
            }
        }
    }
}
